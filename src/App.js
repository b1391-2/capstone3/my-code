//Modules
import { useState, useEffect } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

//Components
import AppNavbar from './components/AppNavbar';
import Footer from './components/Footer';

//Pages
import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';
import ShoppingPage from './pages/ShoppingPage';
import ErrorPage from './pages/ErrorPage';
import MyOrders from './pages/MyOrders';
import MyOrdersAll from './pages/MyOrdersAll';
import MyCart from './pages/MyCart';
import MyProfile from './pages/MyProfile';
import Users from './pages/Users.js';
import Logout from './pages/Logout'
//UserContext

import { UserProvider } from './UserContext';
  
  

export default function App() {

  const [user, setUser] = useState({
    id: null,
    email: null
  })

  //Function for clearing localStorage on Logout
  const unsetUser = () =>{
    localStorage.clear()
    setUser({
      id: null,
      isAdmin: null
    })
  }
 
  useEffect(() => {
    fetch("https://ayeah-ecommerce-booking.herokuapp.com/ecommerce/users/details", {
      method: "GET",
      headers: {
        "Authorization": `Bearer ${localStorage.getItem("token")}`
      }
    }).then(response => response.json()).then(result => {
      if (typeof result._id !== undefined) {
        //console.log(`if`);
        setUser({
          id: result._id,
          isAdmin: result.isAdmin
        })
      }
    })
  }, [])


  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <BrowserRouter>
        <AppNavbar />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/register" component={Register} />
          <Route exact path="/products" component={ShoppingPage} />
          <Route exact path="/orders" component={MyOrders} />
          <Route exact path="/all-orders" component={MyOrdersAll} />
          <Route exact path="/profile" component={MyProfile} />
          <Route exact path="/mycart" component={MyCart} />
          <Route exact path="/users" component={Users} />
          <Route exact path="/logout" component={Logout} />
          <Route component={ErrorPage} />
        </Switch>
        <Footer />
      </BrowserRouter>
    </UserProvider>
    
  );
}