import { Fragment, useState, useEffect, useContext } from 'react'; 
import { Redirect } from 'react-router-dom';
import ProductCardAdmin from './../components/ProductCardAdmin';
import ProductCardUser from './../components/ProductCardUser';

import UserContext from './../UserContext';

export default function ShoppingPage () {

	const { user } = useContext(UserContext);

	const [userName, setUserName] = useState("");
	const [products, setProducts] = useState([]);
	const [productsActive, setProductsActive] = useState([]);

	useEffect(() => {

	const fetchName = () => {fetch("https://ayeah-ecommerce-booking.herokuapp.com/ecommerce/users/details", {
			method: "GET",
			headers: {
				"Authorization" : `Bearer ${localStorage.getItem("token")}`
			}
		}).then(response => response.json()).then(response => {
			setUserName(response.firstName)
		})
	}

	const fetchProducts = () => {
		fetch(`https://ayeah-ecommerce-booking.herokuapp.com/ecommerce/products/`).then(response => response.json()).then(result => {
			setProducts(result); 		
		})
	}

	const fetchProductActive = () => {
		fetch(`https://ayeah-ecommerce-booking.herokuapp.com/ecommerce/products/active-products`).then(response => response.json()).then(result => {
			setProductsActive(result); 		
		})
	}

	fetchName();	
	fetchProducts();
	fetchProductActive();
	
},[])

	const fetchProductAdmin = () => {
		fetch(`https://ayeah-ecommerce-booking.herokuapp.com/ecommerce/products/`).then(response => response.json()).then(result => {
			setProducts(result); 		
		})
	}

	return (
	<Fragment>
		{
			(user.id !== null) ?
		<Fragment>
			 <header className="bg-dark p-1">
        		<div className="container">
         			<div className="text-center text-white d-flex justify-content-center">
             	   <h1 className="display-4 fw-bolder p-2">Welcome back {userName}! Shop now!</h1>
          	  		</div>
       			</div>
  		  	</header>
  		  	<div className="d-flex flex-wrap justify-content-center">
  			{
  				(user.isAdmin === true) ?
  				<ProductCardAdmin productProperty={products} fetchProducts={fetchProductAdmin}/>
  			:	
  				<ProductCardUser productProperty={productsActive}/>
  			}
  			</div>
		</Fragment>
		:
		<Redirect to="/" />
		}
	</Fragment>
		
	)
}

