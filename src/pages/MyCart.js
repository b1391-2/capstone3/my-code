import { useContext, useEffect, useState, Fragment } from 'react';
import { Table, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { useHistory, Redirect } from 'react-router-dom';

import UserContext from './../UserContext';

export default function MyCart () {

	const { user } = useContext(UserContext); 

	let history = useHistory();

	const [ products, setProducts ] = useState([]);
	const [ productList, setProductList] = useState([]);
	const [ amount, setTotalAmount ] = useState(0);
	
	const [ productLength, setProductLength ] = useState(0);
	const [ orderId, setOrderId ] = useState(""); 

	useEffect(() => {
		fetchCart();

		function incrementQuantity (productId) {
			fetch(`https://ayeah-ecommerce-booking.herokuapp.com/ecommerce/orders/adjust-cart-items/${productId}/inc`, {
				method: "POST",
				headers: {
					"Authorization" : `Bearer ${localStorage.getItem("token")}`
				}
			}).then(response => response.json()).then( response => {
				fetchCart()
			})
		}

		function decrementQuantity (productId) {
			fetch(`https://ayeah-ecommerce-booking.herokuapp.com/ecommerce/orders/adjust-cart-items/${productId}/dec`, {
				method: "POST",
				headers: {
					"Authorization" : `Bearer ${localStorage.getItem("token")}`
				}
			}).then(response => response.json()).then( response => {
				fetchCart()
			})
		}
		
		function removeToCart (productId, index) {
			fetchCart()
			fetch(`https://ayeah-ecommerce-booking.herokuapp.com/ecommerce/orders/${productId}/${index}/remove-to-cart`,{
				method: "POST",
				headers:  {
					"Authorization" : `Bearer ${localStorage.getItem("token")}`
				}
			}).then(response => response.json()).then(response => {
				console.log(response);
				if (response) {
					Swal.fire({
						title: "Remove to Cart",
						icon: "success",
						text: "Item successfully removed"
					})
					fetchCart();

				} else {
					Swal.fire({
						title: "Error",
						icon: "error",
						text: "Something went wrong"
					})
					fetchCart();
				}
			})
		}

		const productData = products.map((product, index) => {
			//console.log(index);
			const {productName, productPrice, productQuantity, productId, subtotal} = product
			return (
				<tr>
					<td>{productName}</td>
					<td>Php {productPrice}</td>
					<td>
					<Button className="btn btn-outline-dark" 
							size="sm"
							variant="light"
							onClick={() => {
								decrementQuantity(productId)
							}}>-</Button>
					{"  "+ productQuantity + "  "}
					<Button className="btn btn-outline-dark" 
							size="sm"
							variant="light"
							onClick={() => {
								incrementQuantity(productId)
							}}>+</Button>
					</td>
					<td>Php {subtotal}</td>
					<td>
						<Button className="btn btn-outline-dark" 
								size="sm" 
								variant="light"
								onClick={() => {
									removeToCart(productId, index)
								}}
								>Remove to Cart</Button>
					</td>
				</tr>
			)
		})
		setProductList(productData);
	},[products])

	function fetchCart () {
		fetch(`https://ayeah-ecommerce-booking.herokuapp.com/ecommerce/orders/retrieve-cart`, {
			method: "GET",
			headers: {
				"Authorization" : `Bearer ${localStorage.getItem("token")}`
			}
		}).then(response => response.json()).then(response => {
			//console.log(response.orderedProducts.length);
			const orders = response.orderedProducts
			if (response.isFulfilled === false) {
				setProductLength(response.orderedProducts.length)
				setProducts(orders)
				setTotalAmount(response.totalAmount)
				setOrderId(response._id)
			}
		})
	}

	function IssueCart () {
		fetch(`https://ayeah-ecommerce-booking.herokuapp.com/ecommerce/orders/create-order`, {
			method: "POST",
			headers: {
				"Authorization" : `Bearer ${localStorage.getItem("token")}`
 			}
		}).then(response => response.json()).then(response => {
			if (response) {
				return response
			}
		})
	}

	function checkOut (orderId) {
		fetch(`https://ayeah-ecommerce-booking.herokuapp.com/ecommerce/orders/${orderId}/check-out`, {
			method: "PUT",
			headers: {
				"Authorization" : `Bearer ${localStorage.getItem("token")}`
			}
		}).then(response => response.json()).then(response => {
			if (response) {	
				Swal.fire({
					title: "Check Out",
					icon: "success",
					text: "Item Checked Out! Thanks for Shopping!"
				})
				IssueCart()
				history.push("/products")
			} else {
				Swal.fire({
					title: "Error",
					icon: "error",
					text: "Something went wrong!"
				})
			}
		})
	}



	return (
		<Fragment>
			{
				(user.id !== null) ?
					<div>
						<div className="bg-dark p-2">
							<h1 className="display-4 fw-bolder p-2 text-white d-flex justify-content-center">My Cart</h1>
						</div>
						{
						  (productLength !== 0) ?
						  <div className="p-5">
						  	<div className="text-right p-3">
						  		<Button size="sm" variant="light" 
						  				className="btn-outline-dark"
						  				onClick={() => {
						  					checkOut(orderId)
						  				}}
						  				>Check Out</Button>	
						  	</div>
						  	<Table striped bordered hover>
						  		<thead>
						  			<tr>
						  				<th>Product Name</th>
						  				<th>Price</th>
						  				<th>Quantity</th>
						  				<th>Subtotal</th>
						  				<th>Status</th>
						  			</tr>
						  		</thead>
						  		<tbody>
						  		 {productList}
						  		 <tr>
						  			 <td></td>
						  			 <td></td>
						  		 	<td>Total Price:</td>
						  		 	<td>Php {amount}</td>
						  		 	<td></td>
						  		 </tr>
						  		</tbody>
						  	</Table>
						  </div>	
						  :
						  <div className="d-flex justify-content-center">
						  		<h1 className="p-5 m-5">No Items on Cart Yet</h1>
						  </div>
						}
					</div>
				:
					<Redirect to="/" />
			}
		</Fragment>
	)
}