import { useState, useEffect } from 'react';
import { Table, Button } from 'react-bootstrap'
import Swal from 'sweetalert2';

export default function Users () {

	const [ users, setUsers ] = useState([]);
	const [ userDetails, setUserDetails ] = useState([]);

	useEffect(() => {

		const fetchUsers = () => {
			fetch(`https://ayeah-ecommerce-booking.herokuapp.com/ecommerce/users`, {
				method: "GET",
				headers: {
					"Authorization" : `Bearer ${localStorage.getItem("token")}`
				}
			}).then(response => response.json()).then(response => {
				//console.log(response)
				setUsers(response);
			})
		}

		fetchUsers();

		function enableAdmin (userId){
			fetch(`https://ayeah-ecommerce-booking.herokuapp.com/ecommerce/users/${userId}/setToAdmin`, {
				method: "PUT",
				headers: {
					"Authorization" : `Bearer ${localStorage.getItem("token")}`
				}
			}).then(response => response.json()).then(response => {
				if (response) {
					Swal.fire({
						title: "Set to Admin",
						icon: "success",
						text: "User Successfully set to Admin"
					})
					fetchUsers();
				} else {
					Swal.fire({
						title: "Error",
						icon: "error",
						text: "Something went wrong."
					})
				}
			})
		}

		function disableAdmin (userId){
			fetch(`https://ayeah-ecommerce-booking.herokuapp.com/ecommerce/users/${userId}/unsetToAdmin`, {
				method: "PUT",
				headers: {
					"Authorization" : `Bearer ${localStorage.getItem("token")}`
				}
			}).then(response => response.json()).then(response => {
				if (response) {
					Swal.fire({
						title: "Unset to Admin",
						icon: "success",
						text: "User Successfully unset to Admin"
					})
					fetchUsers();
				} else {
					Swal.fire({
						title: "Error",
						icon: "error",
						text: "Something went wrong."
					})
				}
			})
		}

		const userDtl = users.map((user) => {
			//console.log(user);
			const { _id, firstName, lastName, email, isAdmin, mobileNo} = user
			return (
				<tr>
					<td>{_id}</td>
					<td>{firstName + " " + lastName}</td>
					<td>{email}</td>
					<td>{mobileNo}</td>
					{
						(isAdmin) ?
						<td>Admin</td>
					:
						<td>Not Admin</td>
					}
					{
						(isAdmin) ?
						<td>
							<Button className="btn btn-outline-dark" 
									size="sm" 
									variant="light"
									onClick={() => {
										disableAdmin(_id);
									}}>Disable Admin</Button>
						</td>
					:
						<td>
							<Button className="btn btn-outline-dark"
									size="sm" 
									variant="light"
									onClick={() => {
										enableAdmin(_id);
									}}>Enable Admin</Button>
						</td>
					}
				</tr>
			)
		})
		setUserDetails(userDtl);
	}, [users])


	return (
		<div>
			<div className="bg-dark p-2">
				<h1 className="display-4 fw-bolder p-2 text-white d-flex justify-content-center">All Users | Admin</h1>
			</div>
			<div className="m-4 p-3">
				<Table striped bordered hover>
					<thead>
						<tr>
							<th>Registration No.</th>
							<th>Name</th>
							<th>Email</th>
							<th>Contact No.</th>
							<th>Status</th>
							<th>Remarks</th>
						</tr>
					</thead>
					<tbody>
						{userDetails}
					</tbody>
				</Table>
			</div>
		</div>
	)
}