import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container, Modal, Spinner } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';

import UserContext from './../UserContext';

import Swal from 'sweetalert2';

export default function Login () {

	const { user, setUser } = useContext(UserContext);

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isDisabled, setIsDisabled] = useState(false);

	useEffect(() => {
		if (email !== "" && password !== "") {
			//console.log(`if`)
			setIsDisabled(false);
		} else {
			//console.log(`else`)
			setIsDisabled(true);
		}
	}, [email, password])
	//console.log(user);

	const [logging, setLogging] = useState(false);
	const openLog = () => setLogging(true);
	const closeLog = () => setLogging(false);
	

	function logIn (event) {
		event.preventDefault()
		
		fetch("https://ayeah-ecommerce-booking.herokuapp.com/ecommerce/users/login", {
			method: "POST",
			headers: {
				"Content-Type" : "application/json"
			},
			body: JSON.stringify({
				email : email,
				password : password
			})
		}).then(response => response.json()).then(response =>{
			//console.log(response);
		
		if (response !== false){
				//store the data in local storage
				localStorage.setItem("token", response.access)
				userDetails(response.access)
				//closeLog()
				fetch(`https://ayeah-ecommerce-booking.herokuapp.com/ecommerce/orders/cart-exists`,{
					method: "GET",
					headers: {
						"Authorization" : `Bearer ${response.access}`
					}
				}).then(response => response.json()).then(result => {
					if (result) {
						fetch(`https://ayeah-ecommerce-booking.herokuapp.com/ecommerce/orders/create-order`, {
							method: "POST",
							headers: {
								"Authorization" : `Bearer ${response.access}`
							}
						}).then(response => response.json()).then(result => {
							if (result) {
								Swal.fire({
									title: "Login Successful",
									icon: "success",
									text: "Welcome to aYeah's!"
								})
							}
						})
					} else {
						Swal.fire({
							title: "Login Successful",
							icon: "success",
							text: "Welcome to aYeah's!"
						})
					}
				})

			} else {
				closeLog()
				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check your login details and try again!"
				})

			}
		})

		setEmail("");
		setPassword("");
		
	}

	const userDetails = (token) => {
		fetch("https://ayeah-ecommerce-booking.herokuapp.com/ecommerce/users/details", {
			method: "GET",
			headers: {
				"Authorization" : `Bearer ${token}`
			}
		}).then(response => response.json()).then(result => {
			//console.log(result);
			setUser({
				id: result._id,
				isAdmin: result.isAdmin
			});
		})
	}

	return (
		(user.id !== undefined && user.isAdmin !== undefined) ?
		<Redirect to="/" />
		:
	<Container>
		<Form className="m-5 p-5">
		  <Form.Group className="mb-3" controlId="formBasicEmail">
		    <Form.Label>Email address</Form.Label>
		    <Form.Control 	type="email" 
		    				placeholder="Enter email"
		    				value={email}
		    				onChange={ (event) => {
		    					setEmail(event.target.value)
		    				}} />
		  </Form.Group>

		  <Form.Group className="mb-3" controlId="formBasicPassword">
		    <Form.Label>Password</Form.Label>
		    <Form.Control 	type="password" 
		    				placeholder="Password"
		    				value={password}
		    				onChange={ (event) => {
		    					setPassword(event.target.value)
		    				}} />
		  </Form.Group>
		  <Form.Group className="mb-3" controlId="formBasicCheckbox">
		  </Form.Group>
		  <Button 	className="btn-outline-dark"
		  			type="submit" 
		  			variant="light"
		  			disabled={isDisabled}
		  			onClick={(event) => {
		  				openLog()
		  				logIn(event);
		  			}}
		  			>
		    Submit
		  </Button>
		</Form>


	{/*Modal for Logging in while loading*/}
		<Modal show={logging} onHide={closeLog}>
		  <Button variant="light" disabled>		    
		    Loading
		    <Spinner
		      as="span"
		      animation="grow"
		      size="sm"
		      role="status"
		      aria-hidden="true"
		    />
		    <Spinner
		      as="span"
		      animation="grow"
		      size="sm"
		      role="status"
		      aria-hidden="true"
		    />
		    <Spinner
		      as="span"
		      animation="grow"
		      size="sm"
		      role="status"
		      aria-hidden="true"
		    />
		  </Button>
		</Modal>
	</Container>
	)

}