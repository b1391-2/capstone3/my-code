import { useState, useContext, Fragment } from 'react';
import { Table } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import UserContext from './../UserContext';

export default function MyProfile () {

	const { user } = useContext(UserContext)

	const [ firstName, setFirstName ] = useState("");
	const [ lastName, setLastName ] = useState("");
	const [ contact, setContact ] = useState("");
	const [ email, setEmail ] = useState("");

	let wholeName = `${firstName} ${lastName}`;

	fetch("https://ayeah-ecommerce-booking.herokuapp.com/ecommerce/users/details", {
			method: "GET",
			headers: {
				"Authorization" : `Bearer ${localStorage.getItem("token")}`
			}
		}).then(response => response.json()).then(response => {
			//console.log(response)
			setFirstName(response.firstName);
			setLastName(response.lastName)
			setContact(response.mobileNo);
			setEmail(response.email)
		})

	

	return (
		<Fragment>
			{
				(user.id !== null) ?
					<div>
						<div className="bg-dark p-2">
							<h1 className="display-4 fw-bolder p-2 text-white d-flex justify-content-center">My Profile</h1>
						</div>
						<div className="p-4">
							<Table striped bordered hover>
									<thead>
										<tr>
											<th>Name</th>
											<th>Contact</th>
											<th>E-mail Address</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>{wholeName}</td>
											<td>{contact}</td>
											<td>{email}</td>
										</tr>
									</tbody>
							</Table>
						</div>	
					</div>
				:
					<Redirect to="/" />
			}
		</Fragment>
	)
}