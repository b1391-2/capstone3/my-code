//Module
import { Form, Button, Container } from 'react-bootstrap';
import { Redirect, useHistory } from 'react-router-dom';
import { useState, useEffect } from 'react';
import { useContext } from 'react';
import Swal from 'sweetalert2';
import UserContext from './../UserContext';

export default function Register () {

	let history = useHistory();

	const { user } = useContext(UserContext);

	const [ firstName, setFirstName ] = useState("");
	const [ lastName, setLastName ] = useState("");
	const [ contactNo, setContactNo ] = useState("");
	const [ email, setEmail ] = useState("");
	const [ password, setPassword ] = useState("");
	const [ confirmPassword, setConfirmPassword] = useState("");
	const [ isDisabled, setIsDisabled ] = useState(true);
	
	useEffect(() => {
		if ((firstName !== "" && lastName !== "" && contactNo !== "" && email !== "" & password !== "" & confirmPassword !== "") && (password === confirmPassword)) {
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}
	}, [firstName, lastName, contactNo, email, password, confirmPassword])


	function register (event) {
		event.preventDefault();
		fetch("https://ayeah-ecommerce-booking.herokuapp.com/ecommerce/users/email-exists", {
			method: "POST",
			headers: {
				"Content-Type" : "application/json"
			},
			body: JSON.stringify({
				email : email
			}) 
		}).then(response => response.json()).then((json) => {
			if (json) {
					fetch("https://ayeah-ecommerce-booking.herokuapp.com/ecommerce/users/register", {
					method: "POST",
					headers: {
						"Content-Type" : "application/json"
					},
					body: JSON.stringify({
						firstName : firstName,
						lastName : lastName,
						email : email,
						mobileNo : contactNo,
						password : password
					})
				}).then(response => response.json()).then(result => {
				if (result) {
					Swal.fire({
						title: "Registration Successful",
						icon: "success",
						text: "Welcome to aYeah's Shop!"
					})
					history.push("/login");
					setFirstName("");
					setLastName("");
					setEmail("");
					setContactNo("");
					setPassword("");
					setConfirmPassword("");
				} else {
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again!"
						})
				}
			})

			} else {
				Swal.fire({
				title: "Email Exists",
				icon: "error",
				text: "Please try another email!"
				})
			}
		})		
	}

	return (
		(user.id !== undefined && user.isAdmin !== undefined) ?
		<Redirect to="/" />
		:
			<Container>
	 			<h3 className="border rounded text-center m-3 p-3">Register Now and Enjoy Privileges and Discounts!</h3>
	 			<div className="border rounded justify-content-left m-3 p-3">
					<Form >
					<Form.Group className="mb-3" controlId="formFirstName">
					  <Form.Label>First Name</Form.Label>
					  <Form.Control type="text"
					  		onChange={(event) => {
					  			setFirstName(event.target.value)
					  		}} 
					  		value={firstName}
					  		placeholder="Enter First Name" />
					</Form.Group>

					<Form.Group className="mb-3" controlId="formLastName">
					  <Form.Label>Last Name</Form.Label>
					  <Form.Control type="text"
					  		onChange={(event) => {
					  			setLastName(event.target.value)
					  		}} 
					  		value={lastName}
					  		placeholder="Enter Last Name" />
					</Form.Group>

					<Form.Group className="mb-3" controlId="formContactNo">
					  <Form.Label>Contact Number</Form.Label>
					  <Form.Control type="text"
					  		onChange={(event) => {
					  			setContactNo(event.target.value)
					  		}} 
					  		value={contactNo}
					  		placeholder="Enter Contact Number" />
					</Form.Group>

					  <Form.Group className="mb-3" controlId="formEmail">
					    <Form.Label>Email address</Form.Label>
					    <Form.Control type="email"
					    		onChange={(event) => {
					    			setEmail(event.target.value)
					    		}} 
					    		value={email}
					    		placeholder="Enter email" />
					  </Form.Group>

					  <Form.Group className="" controlId="formPassword">
					    <Form.Label>Password</Form.Label>
					    <Form.Control type="password"
					    		onChange={(event) => {
					    			setPassword(event.target.value)
					    		}} 
					    		value={password}
					    		placeholder="Password" />
					  </Form.Group>
					  <Form.Group className="mb-3" controlId="formBasicCheckbox">
					  </Form.Group>

					  <Form.Group className="" controlId="formPassword">
					    <Form.Label>Confirm Password</Form.Label>
					    <Form.Control type="password"
					    		onChange={(event) => {
					    			setConfirmPassword(event.target.value)
					    		}} 
					    		value={confirmPassword}
					    		placeholder="Confirm Password" />
					  </Form.Group>
					  <Form.Group className="mb-3" controlId="formBasicCheckbox">
					  </Form.Group>
					  <Button 	variant="light" 
					  			className="btn btn-outline-dark" 
					  			type="submit"
					  			disabled={isDisabled}
					  			onClick={(event) => {
					  				register(event)
					  			}}
					  			>Submit</Button>
					</Form>
	 			</div>
	 		</Container>
	)
}