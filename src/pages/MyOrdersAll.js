import { useState, useEffect, useContext, Fragment } from "react";
import { Table } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';

import UserContext from './../UserContext';

export default function MyOrdersAll () {

	const { user } = useContext(UserContext);
	
	const [ orders, setOrders ] = useState([]);
	const [ indivOrders, setIndivOrders] = useState([]);

	useEffect(() => {

		const fetchOrders = () => {
			fetch(`https://ayeah-ecommerce-booking.herokuapp.com/ecommerce/orders/`).then(response => response.json()).then(response => {
				//console.log(response);
				setOrders(response);
			})
		}

		fetchOrders();

		function ConvertMonth (Month) {
			let MonthConverted;
				if (Month === "01") {
				MonthConverted = "Jan"
				return MonthConverted
			} else if (Month === "02"){
				MonthConverted = "Feb"
				return MonthConverted
			} else if (Month === "03"){
				MonthConverted = "Mar"
				return MonthConverted
			} else if (Month === "04") {
				MonthConverted = "Apr"
				return MonthConverted
			} else if (Month === "05") {
				MonthConverted = "May"
				return MonthConverted
			} else if (Month === "06") {
				MonthConverted = "Jun"
				return MonthConverted
			} else if (Month === "07") {
				MonthConverted = "Jul"
				return MonthConverted
			} else if (Month === "08") {
				MonthConverted = "Aug"
				return MonthConverted
			} else if (Month === "09") {
				MonthConverted = "Sept"
				return MonthConverted
			} else if (Month === "10") {
				MonthConverted = "Oct"
				return MonthConverted
			} else if (Month === "11") {
				MonthConverted = "Nov"
				return MonthConverted
			} else {
				MonthConverted = "Dec"
				return MonthConverted
			};
		}

		function ConvertedDate (wholeDate) {
			let Year = wholeDate.slice(0,4);
			let Month = wholeDate.slice(5,7);
			let Day = wholeDate.slice(8,10);

			let Hour = wholeDate.slice(11,13);
			let Minute = wholeDate.slice(14,16);
			
			let wholeStatement;

			wholeStatement = `${ConvertMonth(Month)} ${Day}, ${Year} at ${Hour}:${Minute}`;

			return wholeStatement
		}

		const individualOrder = orders.map((order) => {
			const { userName, totalAmount, purchasedOn, orderedProducts, isFulfilled, _id} = order
			return (
				<tr>
					<td>
					{
					 	userName				
					}
					</td>
					<td>
					{
						orderedProducts.map((product) => {
							const { productName, productQuantity, subtotal } = product
							return (
								<tr>{`Product Name: ${productName} Qty: ${productQuantity} Subtotal: Php${subtotal}`}</tr>
							)
						})
					}
					</td>
					<td>{totalAmount}</td>
					<td>{ConvertedDate(purchasedOn)}</td>
					<td>{_id}</td>
					{
						(isFulfilled) ?
						<td>Checked Out</td>
					:
						<td>Not Yet Checked Out</td>
					}
				</tr>
			)
		})

		setIndivOrders(individualOrder)
	},[orders])


	return (
		<Fragment>
			{
				(user.id !== null) ?
					<div>
						<div className="bg-dark p-2">
							<h1 className="display-4 fw-bolder p-2 text-white d-flex justify-content-center">All Users | Admin</h1>
						</div>
						<div className="m-4 p-3">
							<Table striped bordered hover>
								<thead>
									<tr>
										<th>Purchased by</th>
										<th>Products</th>
										<th>Amount</th>
										<th>Date Purchased</th>
										<th>Reference No.</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
									{indivOrders}
								</tbody>
							</Table>
						</div>
					</div>
				:
					<Redirect to="/" />
			}
		</Fragment>	
	)
}