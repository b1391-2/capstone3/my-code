import { Card, Button } from 'react-bootstrap';
import { Fragment, useState, useEffect } from 'react';
import Swal from 'sweetalert2';

export default function ProductCardUser (props) {

	const { productProperty } = props
	const [productCard, setProductCard] = useState([]);

	useEffect(() => {

		function addToCart(productId) {
			//console.log(`working`)

			// find the cart first for the user then if a product is found on the user return a message saying that the product is already added

			fetch(`https://ayeah-ecommerce-booking.herokuapp.com/ecommerce/orders/retrieve-cart`, {
			method: "GET",
			headers: {
				"Authorization" : `Bearer ${localStorage.getItem("token")}`
			}
			}).then(response => response.json()).then(response => {
				const { orderedProducts } = response
				
				let productFinder = orderedProducts.find((product) =>{ return product.productId === productId})

				if (productFinder !== undefined) {
					Swal.fire({
						title: "Item Already Added to Cart",
						icon: "error",
						text: "Please check cart."
					})
				} else {
					fetch(`https://ayeah-ecommerce-booking.herokuapp.com/ecommerce/orders/${productId}/add-to-cart`, {
						method: "POST",
						headers: {
							"Content-Type" : "application/json",
							"Authorization" : `Bearer ${localStorage.getItem("token")}`
						}
					}).then(response => response.json()).then(response => {
						if (response) {
							Swal.fire({
								title: "Add to Cart",
								icon: "success",
								text: "Item successfully added to cart!"
							})
						} else {
							Swal.fire({
								title: "Error",
								icon: "error",
								text: "Something went wrong"
							})
						}
					})
				}	
			})	
		}

		const productList = productProperty.map((product) => {
			const { _id, productName, productDescription, productPrice} = product

			return (
			<Fragment>
				<Card className="m-4" style={{ width: '12rem' }}>
				  <Card.Body>
				    <Card.Title className="text-center">{productName}</Card.Title>
				    <Card.Text>{productDescription}</Card.Text>
				   <Card.Title>PHP {productPrice}</Card.Title>
				   
				   <div className="text-center p-2">
		   			<Button variant="light" size="sm" 
		   				className="font-weight-bold btn btn-outline-dark"
		   				onClick={() => {
		   					addToCart(_id)
		   				}}>Add to Cart</Button>
		  		 	</div>
				  </Card.Body>
				</Card>	
			</Fragment>
					)
			}) 
		setProductCard(productList)
	},[productProperty])
	
	
	return (
		<Fragment>
			{
				productCard
			}
		</Fragment>
	)


}