//Modules
import { Navbar, Container, Nav, Modal, Form, Button } from 'react-bootstrap';
import { Fragment, useContext, useState } from 'react';
import { NavLink } from 'react-router-dom';
import Swal from 'sweetalert2';

//Import the Bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css'; 

//Import UserContext
import UserContext from './../UserContext';

export default function AppNavbar () {

	const { user } = useContext(UserContext);

	const [showCreate, setShowCreate] = useState(false);
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const openCreate = () => setShowCreate(true);
	const closeCreate = () => setShowCreate(false);

	const logInNav = () => {
		if (user.id !== null && user.isAdmin === true) {
		return (	
			<Fragment>
				<Nav.Link as={NavLink} to="/products">Products</Nav.Link>
				<Nav.Link as={NavLink} to="/profile">My Profile</Nav.Link>
				<Nav.Link as={NavLink} to="/users">Users</Nav.Link>
				<Nav.Link as={NavLink} to="/mycart">My Cart</Nav.Link>
				<Nav.Link as={NavLink} to="/orders">My Orders</Nav.Link>
				<Nav.Link as={NavLink} to="/all-orders">All Orders</Nav.Link>
				<Nav.Link onClick={() => {openCreate()}}>Create Product</Nav.Link>
				<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
			</Fragment>)
		} else if (user.id !== null && user.isAdmin === false) {
		return (
			<Fragment>
				<Nav.Link as={NavLink} to="/products">Products</Nav.Link>
				<Nav.Link as={NavLink} to="/profile">My Profile</Nav.Link>
				<Nav.Link as={NavLink} to="/orders">My Orders</Nav.Link>
				<Nav.Link as={NavLink} to="/mycart">My Cart</Nav.Link>
				<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
			</Fragment>)
		} else {
		return (
			<Fragment>
				<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
				<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
			</Fragment>)
		}
	}

	function createProduct () {
		fetch(`https://ayeah-ecommerce-booking.herokuapp.com/ecommerce/products/register`,{
			method: "POST",
			headers: {
				"Content-Type" : "application/json",
				"Authorization" : `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				productName: name,
				productDescription: description,
				productPrice: price
			})
		}).then(response => response.json()).then(response => {
			if (response) {
				Swal.fire({
					title: "Create Product",
					icon: "success",
					text: "Item Enabled!"
				})
				closeCreate()
				setName("");
				setDescription("");
				setPrice(0);
			} else {
				Swal.fire({
					title: "Error",
					icon: "error",
					text: "Something went wrong"
				})
			}
		})
	}

	return (
		<Fragment>
			<Navbar bg="light" expand="lg">
			  <Container>
			    <Navbar.Brand >aYeah's</Navbar.Brand>
			    <Navbar.Toggle aria-controls="basic-navbar-nav" />
			    <Navbar.Collapse id="basic-navbar-nav">
			      <Nav className="me-auto d-flex justify-content-around">
			        <Nav.Link as={NavLink} to="/">Home</Nav.Link>
			        {logInNav()}
			      </Nav>
			    </Navbar.Collapse>
			  </Container>
			</Navbar>

			{/*Create Product Modal*/}
			<Modal show={showCreate} onHide={closeCreate}>
				<Modal.Header closeButton>
					<Modal.Title>Create Product</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Form>
						<Form.Group controlId="courseName">
							<Form.Label>Product Name:</Form.Label>
							<Form.Control 	type="text"
											value={name}
											onChange={(event) => {
												setName(event.target.value);
											}}/>
						</Form.Group>
						<Form.Group controlId="courseDescription">
							<Form.Label>Description:</Form.Label>
							<Form.Control 	type="text"
											value={description}
											onChange={(event) => {
												setDescription(event.target.value);
											}}/>
						</Form.Group>
						<Form.Group controlId="price">
							<Form.Label>Price:</Form.Label>
							<Form.Control 	type="number"
											value={price}
											onChange={(event) => {
												setPrice(event.target.value);
											}}/>
						</Form.Group>
						<Button variant="light" 
								className="btn btn-outline-dark m-3" 
								onClick={() => {
									createProduct()
								}}>Submit</Button>
						<Button variant="light" className="btn btn-outline-dark m-3" onClick={closeCreate}>Close</Button>
					</Form>
				</Modal.Body>
			</Modal>
		</Fragment>
	)

}